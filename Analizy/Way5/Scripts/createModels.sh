#!/bin/bash

sciezka=`realpath $0`
sciezka=`dirname $sciezka`
echo $sciezka

mkdir -p Modele
for zakres in Genomy/*; do
	echo $zakres
	mkdir -p Modele/$(basename $zakres)
	for plik in $zakres/*; do
		echo $plik
		wyjscie=Modele/$(basename $zakres)/$(basename $plik)
		python3 $sciezka/../../../Code/HMMSplit.py --brut-force -sn 3 --files $plik -o $wyjscie -t 8 -at --coding 1 --sufiks .store
	done
done
