#!/bin/python

import countNonCyclic as cnc
import pickle, os

genomeToGC={}

def calculateGC(fastaFile, fastaName):
        GC=0
        AllNuc=0
        for line in fastaFile:
            if line[0]=='>':
                continue
            else:
                for c in line:
                    if c=='C' or c=='G':
                        GC+=1
                        AllNuc+=1
                    elif c=='A' or c=='T':
                        AllNuc+=1
        genomeToGC[fastaName]=GC/AllNuc


if __name__=="__main__":
    if not os.path.isdir("GC"):
        if os.path.exists("GC"):
            raise FileExistsError
        os.makedirs("GC")
    cnc.forEachGenome(calculateGC)
    with open("GC/dumpFile.store","wb") as file:
        pickle.dump(genomeToGC,file)


