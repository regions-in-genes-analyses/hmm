#!/bin/bash

sciezka=`realpath $0`
sciezka=`dirname $sciezka`
echo $sciezka

$sciezka/printBICBestModels.sh
$sciezka/createGraphsImages.sh
$sciezka/dumpGC.py
$sciezka/countNonCyclic.py
$sciezka/longestPath.py
$sciezka/realNumberOfStates.py
$sciezka/edgesCount.py
$sciezka/topologyCount.py
$sciezka/diffBetweenProbabilities.py
$sciezka/numberOfPath.py
