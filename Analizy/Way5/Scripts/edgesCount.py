#!/bin/python

import countNonCyclic as cnc
import itertools as iter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle, os

genomeToEdgeCount={}

def countEdges(model):
    """
    Return number of edges in model from start to non silent states and between this states.
    Doesn't count edges from state to the same state, and from non silent end to silent end.
    """
    if model.states[-1].name!="None-end" or model.states[-2].name!="None-start":
        raise RuntimeException("Structure of model is changed. Constants in code are incorrect.")
    matrix=model.dense_transition_matrix()
    edgeCount=0
    #Check edges which begin in vertex i, ehich can be None-start
    for i in range(1,model.state_count()-1):
        #Check edges ending in j, which can be non silent end
        for j in range(0,model.state_count()-2):
            if i==j:
                continue
            if matrix[i,j]!=0:
                edgeCount+=1
    return edgeCount

def pickEdgesCount(fileWithBestModel, fastaName, prefix):
    edgesCounts=[]
    for line in fileWithBestModel:
        with open(os.path.join(prefix,line.split(" ")[-1][:-1]+".store"), "rb") as modelFile:
            model=pickle.load(modelFile)
            edgesCounts.append(countEdges(model))
    genomeToEdgeCount[fastaName]=edgesCounts
    return

if __name__=="__main__":
    cnc.checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)
    cnc.forEachModelGroup(pickEdgesCount)
    plotDataGC=[]
    plotData=[]
    plotLabels=[]
    print(genomeToEdgeCount)
    for genomeName in genomeToGC:
        plotDataGC.append(round(genomeToGC[genomeName],4))
        plotData.append(genomeToEdgeCount[genomeName])
        plotLabels.append(genomeName)

    plt.xlim(0.20,0.70)
    plt.boxplot(plotData, positions=plotDataGC, widths=0.005)

    for j in range(len(plotLabels)):
        an=plt.annotate(plotLabels[j], [plotDataGC[j], max(plotData[j])])
        an.set_fontsize(4)
        an.set_rotation(45)
    plt.ylabel("Number of edges in models.")
    plt.xlabel("GC")
    plt.savefig("Wykresy/edgesCountsBoxplot.png", dpi=500)
    plt.show()



    plt.xlim(0.20,0.70)
    plt.scatter(plotDataGC, np.mean(np.array(plotData), axis=1))

    for j in range(len(plotLabels)):
        an=plt.annotate(plotLabels[j], [plotDataGC[j], np.mean(plotData[j])])
        an.set_fontsize(4)
        an.set_rotation(45)
    plt.ylabel("Mean count of edges in models.")
    plt.xlabel("GC")
    plt.savefig("Wykresy/edgesCountsMeans.png", dpi=500)
    plt.show()

