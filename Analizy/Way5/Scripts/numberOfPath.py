#!/bin/python

import countNonCyclic as cnc
import itertools as iter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle, os

genomeToMeanNumberOfPaths={}

def countPaths(model, L, numberOfNonReal=0):
    """
    Return 1 if there exist path of length L, from start to end. Else return 0.
    """
    numberOfPaths=0
    if model.states[-2].name!="None-start":
        raise RuntimeException("Structure of model is changed. Constants in code are incorrect.")
    startNr=model.state_count()-2
    matrix=model.dense_transition_matrix()
    for permutation in iter.permutations(range(1,model.state_count()-2),L):
        nonRealInThisPerm=0
        if matrix[startNr][permutation[0]]==0:
            continue
        ifBreak=0
        for i in range(1,L):
            if matrix[permutation[i-1]][permutation[i]]==0:
                ifBreak=1
                break
        if ifBreak:
            continue
        if matrix[permutation[-1]][0]==0:
            continue
        for i in range(L):
            if matrix[permutation[i],permutation[i]]<0.9:
                nonRealInThisPerm+=1
        if numberOfNonReal!=nonRealInThisPerm:
            continue
        numberOfPaths+=1
    return numberOfPaths

def countMeanNumberOfPathsInModels(fileWithBestModel, fastaName, prefix):
    sumOfPaths=np.array([0]*4)
    countOfModels=0
    for line in fileWithBestModel:
        countOfModels+=1
        with open(os.path.join(prefix,line.split(" ")[-1][:-1]+".store"), "rb") as modelFile:
            model=pickle.load(modelFile)
            for i in range(1,4):
                for j in range(0, 4-i):
                    sumOfPaths[i]+=countPaths(model,i, numberOfNonReal=j)
    genomeToMeanNumberOfPaths[fastaName]=sumOfPaths/countOfModels

if __name__=="__main__":
    cnc.checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)
    cnc.forEachModelGroup(countMeanNumberOfPathsInModels)
    plotDataGC=[]
    plotData=[]
    plotLabels=[]
    for genomeName in genomeToGC:
        plotDataGC.append(genomeToGC[genomeName])
        plotData.append(genomeToMeanNumberOfPaths[genomeName])
        plotLabels.append(genomeName)
    plotData=np.array(plotData)

    for i in range(1,4):
        plt.grid(True)
        plt.scatter(plotDataGC, plotData[:,i])
    
        for j in range(len(plotLabels)):
            an=plt.annotate(plotLabels[j], [plotDataGC[j], plotData[j,i]])
            an.set_fontsize(4)
            an.set_rotation(45)
        plt.ylabel("Mean number of paths with length {} in models.".format(i))
        plt.xlabel("GC")
        plt.savefig("Wykresy/meanNumberOfPaths{}.png".format(i), dpi=500)
        plt.show()
