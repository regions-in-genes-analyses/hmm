#!/usr/bin/python3

import os, pickle
import matplotlib
if "DISPLAY" not in os.environ:
    matplotlib.use("Agg")
import countNonCyclic as cnc
import itertools as iter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

genomeAndRealNrOfStates={}
GCBoundaries=[0.30, 0.4, 0.5,0.6, 1.0]

def realNumberOfStates(model):
    if model.states[-1].name!="None-end" or model.states[-2].name!="None-start":
        raise RuntimeException("Structure of model is changed. Constants in code are incorrect.")
    matrix=model.dense_transition_matrix()
    realNr=0
    for i in range(1, model.state_count()-2):
        if matrix[i,i]>=0.9:
            realNr+=1
    return realNr

def pickRealNumberOfStates(fileWithBestModel, fastaName, prefix):
    modelsCount=[0]*4
    for line in fileWithBestModel:
        with open(os.path.join(prefix,line.split(" ")[-1][:-1]), "rb") as modelFile:
            model=pickle.load(modelFile)
            modelsCount[realNumberOfStates(model)]+=1
    genomeAndRealNrOfStates[fastaName]=modelsCount

if __name__=="__main__":
    cnc.checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)

    plotData=np.zeros((len(cnc.modelsGroupsList),len(GCBoundaries),4))
    for groupNr in range(len(cnc.modelsGroupsList)):
        group=cnc.modelsGroupsList[groupNr]
        genomeAndRealNrOfStates.clear()
        cnc.forModelGroup(pickRealNumberOfStates,group)
        for genomeName in genomeToGC:
            for i in range(len(GCBoundaries)):
                if genomeToGC[genomeName]<GCBoundaries[i]:
                    plotData[groupNr,i,np.array(genomeAndRealNrOfStates[genomeName],dtype=bool)]+=1
                    break

    print("=====================")
    print("Number in i-th field of printed array is number of models with i real regions.")
    print("'i' is counted from 0, so first field in each row is control and should be 0,")
    print("because there is no posibility to have model with 0 real states.")
    for groupNr in range(len(cnc.modelsGroupsList)):
        group=cnc.modelsGroupsList[groupNr]
        print("=====================")
        print("In group named:",group)
        print("There is following numbers of real states in diffrent ranges of GC:") 
        print("GC 0.0 -",GCBoundaries[0],":",plotData[groupNr,0,:])
        for i in range(1,len(GCBoundaries)):
            print("GC",GCBoundaries[i-1],"-",GCBoundaries[i],":",plotData[groupNr,i,:])

    
#        for i in range(1,4):
#            plt.scatter(plotDataGC, plotData[:,i])
#            plt.grid(True)
#    
#            for j in range(len(plotLabels)):
#                an=plt.annotate(plotLabels[j], [plotDataGC[j], plotData[j,i]])
#                an.set_fontsize(4)
#                an.set_rotation(45)
#            plt.ylabel("Number of models with {} real regions.".format(i))
#            plt.xlabel("GC")
#            plt.savefig("Wykresy/realRegions{}.png".format(i), dpi=500)
#            if "DISPLAY" in os.environ:
#                plt.show()

    
