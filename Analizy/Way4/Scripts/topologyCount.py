#!/bin/python

import os, pickle
import matplotlib
if "DISPLAY" not in os.environ:
    matplotlib.use("Agg")
import countNonCyclic as cnc
import itertools as iter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

genomeToNrOfTopologies={}

def checkIdentityOfTopology(model1, model2):
    """
    Return 1 if model1 is topology identical to model2.
    """
    matrix1=np.array(np.array(model1.dense_transition_matrix(),dtype=bool),dtype=int)
    matrix2=np.array(np.array(model2.dense_transition_matrix(),dtype=bool),dtype=int)
    if model1.states[-1].name!="None-end" or model1.states[-2].name!="None-start" or \
            model2.states[-1].name!="None-end" or model2.states[-2].name!="None-start":
        raise RuntimeException("Structure of model is changed. Constants in code are incorrect.")

    for perm in iter.permutations(range(1,4),3):
        perm=[0]+list(perm)+[model1.state_count()-2, model1.state_count()-1]
        for i in range(model1.state_count()):
            for j in range(model1.state_count()):
                if matrix1[i,j]!=matrix2[perm[i],perm[j]]:
                    break
            else:
                continue
            break
        else:
            return 1
    return 0

def countTopologies(fileWithBestModel, fastaName, prefix):
    uniqueTopologiesSeen=[]
    print("================================")
    for line in fileWithBestModel:
        with open(os.path.join(prefix,line.split(" ")[-1][:-1]+".store"), "rb") as modelFile:
            model=pickle.load(modelFile)
            for uniqModel in uniqueTopologiesSeen:
                if checkIdentityOfTopology(uniqModel, model):
                    break
            else:
                uniqueTopologiesSeen.append(model)
    genomeToNrOfTopologies[fastaName]=len(uniqueTopologiesSeen)


if __name__=="__main__":
    cnc.checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)
    cnc.forEachModelGroup(countTopologies)
    plotDataGC=[]
    plotData=[]
    plotLabels=[]
    for genomeName in genomeToGC:
        plotDataGC.append(round(genomeToGC[genomeName],4))
        plotData.append(genomeToNrOfTopologies[genomeName])
        plotLabels.append(genomeName)

    plt.scatter(plotDataGC, plotData)

    for j in range(len(plotLabels)):
        an=plt.annotate(plotLabels[j], [plotDataGC[j], np.mean(plotData[j])])
        an.set_fontsize(4)
        an.set_rotation(45)
    plt.ylabel("Topology count.")
    plt.xlabel("GC")
    plt.savefig("Wykresy/topologyCount.png", dpi=500)
    if "DISPLAY" in os.environ:
        plt.show()
