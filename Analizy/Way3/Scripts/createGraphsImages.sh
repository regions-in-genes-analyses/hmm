#!/bin/bash

sciezka=`realpath $0`
sciezka=`dirname $sciezka`
echo $sciezka

for zakres in BestModels/*; do
  echo $zakres
  mkdir -p GrafyDot/$(basename $zakres)
  for plik in $zakres/*; do
    echo $plik
    wyjscie=GrafyDot/$(basename $zakres)/$(basename $plik)
    wyjscieObrazow=GrafyPng/$(basename $zakres)/$(basename $plik)
    mkdir -p $wyjscieObrazow
    for nazwaModelu in `cut -d " " -f 2 $plik`; do 
      model=Modele/$(basename $zakres)/$(basename $plik)/$nazwaModelu".store"
      python3 $sciezka/../../../Code/HMMSplit.py --print --dot -od $wyjscie --files $model
      dot -Tpng "$wyjscie/$nazwaModelu.dot" > "$wyjscieObrazow/$nazwaModelu.png"
    done
  done
done

