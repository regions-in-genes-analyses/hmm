#!/bin/python

import itertools as iter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import pickle, os

import countNonCyclic as cnc

symList=["o-","^-","x-"]
colorList=['red', 'green', 'blue']

distrPoints={}
GCBoundaries=[0.30, 0.55, 1.0]

class Omit(Exception):
    pass

def calculateDiffs(model, L, fastaName, setOfPoints, weights=False, numberOfNonReal=0):
    """
    Add to distrPoints points which describes differences beetwen distributions 
    in states on all path of length L.
    L in {2,3}
    """
    if model.states[-2].name!="None-start":
        raise RuntimeException("Structure of model is changed. Constants in code are incorrect.")
    startNr=model.state_count()-2
    matrix=model.dense_transition_matrix()
  
    sumOfWeights=0

    for permutation in iter.permutations(range(1,model.state_count()-2),L):
        nonRealInThisPerm=0
        if matrix[startNr][permutation[0]]==0:
            continue
        pathWeight=matrix[startNr][permutation[0]]
        ifBreak=0
        for i in range(1,L):
            if matrix[permutation[i-1]][permutation[i]]==0:
                ifBreak=1
                break
            pathWeight*=matrix[permutation[i-1]][permutation[i]]
        if ifBreak:
            continue
        if matrix[permutation[-1]][0]==0:
            continue
        pathWeight*=matrix[permutation[-1]][0]
        try:
            #check if all states on path are real regions
            for i in range(L):
                if matrix[permutation[i],permutation[i]]<0.9:
                    nonRealInThisPerm+=1

            if nonRealInThisPerm!=numberOfNonReal:
                raise Omit
            #if permutation describe path
            point=[]
            for i in range(len(permutation)):
                if matrix[permutation[i],permutation[i]]<0.9:
                    continue
                d1=model.states[permutation[i]].distribution.parameters[0]
                for sym in d1:
                    if sym==0:
                        continue
                    x=d1[sym]
                    point.append(x)

            sumOfWeights+=pathWeight
            setOfPoints[tuple(point)]=pathWeight
        except Omit:
            continue
    return sumOfWeights

def createArrayWithDiffPoints(fileWithBestModel, fastaName, prefix, weights=False):
    for line in fileWithBestModel:
        with open(os.path.join(prefix,line.split(" ")[-1][:-1]+".store"), "rb") as modelFile:
            model=pickle.load(modelFile)
            for i in range(2,4):
                if (fastaName,i) not in distrPoints:
                    distrPoints[(fastaName,i)]=[]
                sumOfWeights=0
                setOfPoints={}
                sumOfWeights+=calculateDiffs(model,i, fastaName, setOfPoints, weights=weights, numberOfNonReal=0)
                if i==2:
                    sumOfWeights+=calculateDiffs(model,i+1, fastaName, setOfPoints, weights=weights, numberOfNonReal=1)
                for point in setOfPoints:
                    if weights:
                        distrPoints[(fastaName,i)].append((point, setOfPoints[point]/sumOfWeights))
                    else:
                        distrPoints[(fastaName,i)].append((point, 1.0))

def plot(weights):
    distrPoints.clear()
    cnc.checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)
    cnc.forEachModelGroup(lambda x,y,z: createArrayWithDiffPoints(x,y,z, weights=weights))
    plotData2=[[] for i in range(len(GCBoundaries))]
    plotData3=[[] for i in range(len(GCBoundaries))]
    dataWeights2=[[] for i in range(len(GCBoundaries))]
    dataWeights3=[[] for i in range(len(GCBoundaries))]
    for genomeName in genomeToGC:
        for i in range(len(GCBoundaries)):
            if genomeToGC[genomeName]<=GCBoundaries[i]:
                plotData2[i]+= [list(elem[0]) for elem in distrPoints[(genomeName,2)]]
                plotData3[i]+= [list(elem[0]) for elem in distrPoints[(genomeName,3)]]
                dataWeights2[i]+= [elem[1] for elem in distrPoints[(genomeName,2)]]
                dataWeights3[i]+= [elem[1] for elem in distrPoints[(genomeName,3)]]
                break

    plotData2=[np.array(plotData2[i]) for i in range(len(plotData2))]
    plotData3=[np.array(plotData3[i]) for i in range(len(plotData3))]

    plt.gcf().set_size_inches((10,10))
    for i in range(len(GCBoundaries)):
        if len(plotData2[i])==0:
            continue
        center=np.average(plotData2[i],axis=0, weights=dataWeights2[i])
        center=np.reshape(center,(-1,3))
        print(center)
        for j in range(3):
            plt.plot([1,2],center[:,j],symList[i],color=colorList[j], label="Symbol {} - GC from {} to {}".format(j, 0.0 if i==0 else GCBoundaries[i-1], GCBoundaries[i]))

    plt.legend()
    plt.grid(True)
    plt.xticks([1,2])
    plt.xlabel("Region")
    plt.ylabel("Probability of emission.")
    plt.title("Mean probabilities of emission symbols in diffrent regions - {}.".format("all paths equally weighted" if weights else "weights of paths from one model sum into 1"))
    plt.savefig("Wykresy/distrMean-Len2-{}.png".format("weighted" if weights else "nonWeighted"), dpi=500)
    plt.show()

    print("============")

    plt.gcf().set_size_inches((10,10))
    for i in range(len(GCBoundaries)):
        if len(plotData3[i])==0:
            continue
        center=np.average(plotData3[i],axis=0, weights=dataWeights3[i])
        center=np.reshape(center,(-1,3))
        print(center)
        for j in range(3):
            plt.plot([1,2,3],center[:,j],symList[i],color=colorList[j], label="Symbol {} - GC from {} to {}".format(j, 0.0 if i==0 else GCBoundaries[i-1], GCBoundaries[i]))

    plt.legend()
    plt.grid(True)
    plt.xticks([1,2,3])
    plt.xlabel("Region")
    plt.ylabel("Probability of emission.")
    plt.title("Mean probabilities of emission symbols in diffrent regions - {}.".format("all paths equally weighted" if weights else "weights of paths from one model sum into 1"))
    plt.savefig("Wykresy/distrMean-Len3-{}.png".format("weighted" if weights else "nonWeighted"), dpi=500)
    plt.show()

if __name__=="__main__":
    np.set_printoptions(precision=4, suppress=True)
    plot(False)
    plot(True)
