#!/bin/bash

sciezka=`realpath $0`
sciezka=`dirname $sciezka`
echo $sciezka

if [[ $# < 1 ]] ; then
  echo "First and only argument should be name of topology type (for example: \"base1\", \"base3\")."
  quit
fi

topType=$1

for plik in Genomy/Abecadlo/*; do
	wyjscie=Modele/$topType/$(basename $plik)
	python3 $sciezka/../../../Code/HMMSplit.py --create --files $plik -od $wyjscie -at --coding 1 --sufiks .store -s $sciezka/../../../Start/$topType -e $sciezka/../../../End/$topType -tm $sciezka/../../../Matrix/$topType --name m -cn 10
done
