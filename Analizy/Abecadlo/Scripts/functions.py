
def translateLetterSymbolsOfFunctionaGroups(listOfLetters):
    dictionary={
            "A":"RNAprocessing",
            "B":"Chromatin_structure",
            "C":"Energy_production",
            "D":"Cell_cycle",
            "E":"Amino_acid_transport",
            "F":"Nucleotide_transport",
            "G":"Carbohydrate_transport",
            "H":"Coenzyme_transport",
            "I":"Lipid_transport",
            "J":"Translation_ribosomal",
            "K":"Transcription",
            "L":"Replication_recombination",
            "M":"Cell_wall",
            "N":"Cell_motility",
            "O":"Posttranslational_modification",
            "P":"Inorganic_ion_transport",
            "Q":"Secondary_metabolites",
            "R":"Function prediction",
            "S":"Function_unknown",
            "T":"Signal_transduction",
            "U":"Intracellular_trafficking",
            "V":"Defense_mechanisms",
            "W":"Extracellular_structures",
            "Y":"Nuclear_structure",
            "Z":"Cytoskeleton"}
    return [dictionary[let] for let in listOfLetters]
