#!/bin/bash

sciezka=`realpath $0`
sciezka=`dirname $sciezka`
echo $sciezka

for i in {1..3}; do
	echo "base$i"
	mkdir -p BestModels/base$i
	for genom in Genomy/All/*; do
	    plikZNajlepszymi=BestModels/base$i/$(basename $genom)
	    katalogZModelami=Modele/base$i/$(basename $genom)
	    python3 $sciezka/../../../Code/HMMSplit.py --analizeManyBIC -id $katalogZModelami --files $genom --coding 1 --sufiks .store -nb 1 >$plikZNajlepszymi
	done
done

