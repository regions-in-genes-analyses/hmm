#!/usr/bin/python3

import pickle, os, matplotlib
import itertools as iter
import numpy as np
if "DISPLAY" not in os.environ:
    matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm

import countNonCyclic as cnc

symList=["o-","^-","x-",'+-','s-']
colorList=['red', 'green', 'blue','orange','black']

distrPoints={}
expectedLength={}
GCBoundaries=[0.30, 0.4, 0.5, 0.6, 1.0]
modelsGroupsList=['base2','base3']
class Omit(Exception):
    pass

def calculateDiffs(model, L, fastaName, setOfPoints, weights=False, numberOfNonReal=0):
    """
    Add to distrPoints points which describes differences beetwen distributions 
    in states on all path of length L.
    L in {2,3}
    """
    if model.states[-2].name!="None-start":
        raise RuntimeException("Structure of model is changed. Constants in code are incorrect.")
    startNr=model.state_count()-2
    matrix=model.dense_transition_matrix()
  
    sumOfWeights=0

    for permutation in iter.permutations(range(1,model.state_count()-2),L):
        nonRealInThisPerm=0
        if matrix[startNr][permutation[0]]==0:
            continue
        pathWeight=matrix[startNr][permutation[0]]
        ifBreak=0
        for i in range(1,L):
            if matrix[permutation[i-1]][permutation[i]]==0:
                ifBreak=1
                break
            pathWeight*=matrix[permutation[i-1]][permutation[i]]
        if ifBreak:
            continue
        if matrix[permutation[-1]][0]==0:
            continue
        pathWeight*=matrix[permutation[-1]][0]
        try:
            #check if all states on path are real regions
            for i in range(L):
                if matrix[permutation[i],permutation[i]]<0.9:
                    nonRealInThisPerm+=1

            if nonRealInThisPerm!=numberOfNonReal:
                raise Omit
            #if permutation describe path
            point=[]
            if (L-nonRealInThisPerm,fastaName) in expectedLength:
                raise RuntimeError("Fasta name already in dictionary.")
            expectedLength[(L-nonRealInThisPerm,fastaName)]=[]
            for i in range(len(permutation)):
                if matrix[permutation[i],permutation[i]]<0.9:
                    continue
                expectedLength[(L-nonRealInThisPerm,fastaName)].append(int(1/(1-matrix[permutation[i],permutation[i]])))
                d1=model.states[permutation[i]].distribution.parameters[0]
                for sym in d1:
                    if sym==3:
                        continue
                    x=d1[sym]
                    point.append(x)

            sumOfWeights+=pathWeight
            setOfPoints[tuple(point)]=pathWeight
        except Omit:
            continue
    return sumOfWeights

def createArrayWithDiffPoints(fileWithBestModel, fastaName, prefix, weights=False):
    for line in fileWithBestModel:
        with open(os.path.join(prefix,line.split(" ")[-1][:-1]), "rb") as modelFile:
            model=pickle.load(modelFile)
            for i in range(2,4):
                if (fastaName,i) not in distrPoints:
                    distrPoints[(fastaName,i)]=[]
                sumOfWeights=0
                setOfPoints={}
                sumOfWeights+=calculateDiffs(model,i, fastaName, setOfPoints, weights=weights, numberOfNonReal=0)
                if i==2:
                    sumOfWeights+=calculateDiffs(model,i+1, fastaName, setOfPoints, weights=weights, numberOfNonReal=1)
                for point in setOfPoints:
                    if weights:
                        distrPoints[(fastaName,i)].append((point, setOfPoints[point]/sumOfWeights))
                    else:
                        distrPoints[(fastaName,i)].append((point, 1.0))

def plot(weights, groupName):
    distrPoints.clear()
    expectedLength.clear()
    cnc.checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)
    cnc.forModelGroup(lambda x,y,z: createArrayWithDiffPoints(x,y,z, weights=weights), groupName)
    plotData2=[[] for i in range(len(GCBoundaries))]
    plotData3=[[] for i in range(len(GCBoundaries))]
    dataWeights2=[[] for i in range(len(GCBoundaries))]
    dataWeights3=[[] for i in range(len(GCBoundaries))]
    fileNames2=[[] for i in range(len(GCBoundaries))]
    fileNames3=[[] for i in range(len(GCBoundaries))]
    for genomeName in genomeToGC:
        for i in range(len(GCBoundaries)):
            if genomeToGC[genomeName]<=GCBoundaries[i]:
                plotData2[i]+= [list(elem[0]) for elem in distrPoints[(genomeName,2)]]
                plotData3[i]+= [list(elem[0]) for elem in distrPoints[(genomeName,3)]]
                dataWeights2[i]+= [elem[1] for elem in distrPoints[(genomeName,2)]]
                dataWeights3[i]+= [elem[1] for elem in distrPoints[(genomeName,3)]]
                fileNames2[i]+=[genomeName for elem in distrPoints[(genomeName,2)]]
                fileNames3[i]+=[genomeName for elem in distrPoints[(genomeName,3)]]
                break

    plotData2=[np.array(plotData2[i]) for i in range(len(plotData2))]
    plotData3=[np.array(plotData3[i]) for i in range(len(plotData3))]

    print("=========================")
    print("Files which has following order of probabilities of generating symbol 0.")
    if groupName=='base3':
        orderOfProbability=[[] for i in range(6)]
        for i in range(len(GCBoundaries)):
            #1<2<3 1<3<2 2<1<3 2<3<1 3<1<2 3<2<1
            #one row of plotData3 has 9 values 0-th, 3-th and 6-th describe ppb of symbol 0 in subseqences regions
            if len(plotData3[i])==0:
                continue
            for j in range(len(plotData3[i])):
                if plotData3[i][j,0]<plotData3[i][j,3]:
                    #1<2
                    if plotData3[i][j,3]<plotData3[i][j,6]:
                        #1<2<3
                        orderOfProbability[0].append(fileNames3[i][j])
                    else:
                        #3<2
                        if plotData3[i][j,0]<plotData3[i][j,6]:
                            #1<3<2
                            orderOfProbability[1].append(fileNames3[i][j])
                        else:
                            #3<1<2
                            orderOfProbability[4].append(fileNames3[i][j])
                else:
                    #2<1
                    if plotData3[i][j,3]<plotData3[i][j,6]:
                        #2<3
                        if plotData3[i][j,0]<plotData3[i][j,6]:
                            #2<1<3
                            orderOfProbability[2].append(fileNames3[i][j])
                        else:
                            #2<3<1
                            orderOfProbability[3].append(fileNames3[i][j])
                    else:
                        #3<2<1
                        orderOfProbability[5].append(fileNames3[i][j])
    
        orderOfProbabilityLabel=["1<2<3", "1<3<2", "2<1<3", "2<3<1", "3<1<2", "3<2<1"]
        print("=========================", groupName)
        for i in range(6):
            print(orderOfProbabilityLabel[i],":",orderOfProbability[i])


    orderOfProbability=[[] for i in range(2)]
    for i in range(len(GCBoundaries)):
        #1<2 2<1
        #one row of plotData3 has 6 values 0-th and 3-th describe ppb of symbol 0 in subseqences regions
        if len(plotData2[i])==0:
            continue
        for j in range(len(plotData2[i])):
            if plotData2[i][j,0]<plotData2[i][j,3]:
                #1<2
                orderOfProbability[0].append(fileNames2[i][j])
            else:
                #2<1
                orderOfProbability[1].append(fileNames2[i][j])

    orderOfProbabilityLabel=["1<2", "2<1"]
    print("=========================", groupName)
    for i in range(2):
        print(orderOfProbabilityLabel[i],":",orderOfProbability[i])

    L={2}
    if groupName=="base3":
        L.add(3);
    print("=========================", groupName)
    print("Expected length of each region (calculated based on binomial distribution).")
    for i in L:
        for genomeName in genomeToGC:
            if (i, genomeName) in expectedLength:
                print("Genome:", genomeName, "Region count:", i, "->",expectedLength[(i,genomeName)])
    print("=========================", groupName)
    print("=========================")
    print("=========================")



if __name__=="__main__":
    np.set_printoptions(precision=4, suppress=True)
    for groupName in modelsGroupsList:
        plot(False, groupName)
