#!/usr/bin/python
import pomegranate as pom
import matplotlib.pyplot as plt
import os, pickle

genomeToNonCyclic={}
def checkExistenceOfPlotOutDir():
    if not os.path.isdir("Wykresy"):
        if os.path.exists("Wykresy"):
            raise FileExistsError
        os.makedirs("Wykresy")

def checkNonCyclic(path):
    """ Return 1 when there is no cycle, else return 0."""
    with open(path, "rb") as modelFile:
        model=pickle.load(modelFile)
    matrix=model.dense_transition_matrix()
    numberOfStates=model.state_count()
    visited=[0 for i in range(numberOfStates)]

    def DFS(v):
        """Return 1 if cycle, else return 0."""
        visited[v]=1
        for u in range(numberOfStates):
            if matrix[v,u]==0 or v==u:
                continue
            if visited[u]==1:
                return 1
            if visited[u]==0:
                h=DFS(u)
                if h==1:
                    return 1
        visited[v]=2
        return 0

    for i in range(numberOfStates):
        if visited[i]==0:
            h=DFS(i)
            if h==1:
                return 0
    return 1

def forEachGenome(fun):
    for rangeDir in os.listdir("Genomy"):
        print(rangeDir)
        pathToRangeDir=os.path.join("Genomy",rangeDir)
        for fastaName in os.listdir(pathToRangeDir):
            print(fastaName)
            fastaFile=open(os.path.join(pathToRangeDir,fastaName))
            fun(fastaFile, fastaName)
            fastaFile.close() 

def countNonCyclic(fileWithBestModel, fastaName, prefix):
    nonCyclicModels=0
    allModels=0
    for line in fileWithBestModel:
        allModels+=1
        nonCyclicModels+=checkNonCyclic(os.path.join(prefix,line.split(" ")[-1][:-1]+".store"))
    genomeToNonCyclic[fastaName]=nonCyclicModels/allModels

def forEachModelGroup(fun):
    for rangeDir in os.listdir("Genomy"):
        print(rangeDir)
        pathToRangeDir=os.path.join("Genomy",rangeDir)
        for fastaName in os.listdir(pathToRangeDir):
            with open(os.path.join("BestModels",rangeDir,fastaName)) as fileWithBestModel:
                fun(fileWithBestModel, fastaName, os.path.join("Modele", rangeDir, fastaName))

if __name__=="__main__":
    checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)
    forEachModelGroup(countNonCyclic)
    plotDataGC=[]
    plotDataNonCyclic=[]
    plotLabels=[]
    for genomeName in genomeToGC:
        plotDataGC.append(genomeToGC[genomeName])
        plotDataNonCyclic.append(genomeToNonCyclic[genomeName])
        plotLabels.append(genomeName)
    plt.scatter(plotDataGC, plotDataNonCyclic)
    for i in range(len(plotLabels)):
        an=plt.annotate(plotLabels[i], [plotDataGC[i], plotDataNonCyclic[i]])
        an.set_fontsize(4)
        an.set_rotation(45)
    plt.ylabel("Number of non cyclic models.")
    plt.xlabel("GC")
    plt.savefig("Wykresy/numberOfCyclicModels-vs-GC.png", dpi=500)
    plt.show()
