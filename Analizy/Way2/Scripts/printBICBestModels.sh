#!/bin/bash

sciezka=`realpath $0`
sciezka=`dirname $sciezka`
echo $sciezka

for zakres in Genomy/*; do
  echo $zakres
  mkdir -p BestModels/$(basename $zakres)
  for plik in $zakres/*; do
    echo $plik
    plikZNajlepszymi=BestModels/$(basename $zakres)/$(basename $plik)
    katalogZModelami=Modele/$(basename $zakres)/$(basename $plik)
    python3 $sciezka/../../../Code/HMMSplit.py --bfatoBIC -id $katalogZModelami --files $plik --coding 1 --sufiks .store -nb 1 >$plikZNajlepszymi
  done
done

