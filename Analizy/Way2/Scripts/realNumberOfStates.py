#!/bin/python

import countNonCyclic as cnc
import itertools as iter
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pickle, os

genomeAndRealNrOfStates={}

def realNumberOfStates(model):
    if model.states[-1].name!="None-end" or model.states[-2].name!="None-start":
        raise RuntimeException("Structure of model is changed. Constants in code are incorrect.")
    matrix=model.dense_transition_matrix()
    realNr=0
    for i in range(1, model.state_count()-2):
        if matrix[i,i]>=0.9:
            realNr+=1
    return realNr

def pickRealNumberOfStates(fileWithBestModel, fastaName, prefix):
    modelsCount=[0]*4
    for line in fileWithBestModel:
        with open(os.path.join(prefix,line.split(" ")[-1][:-1]+".store"), "rb") as modelFile:
            model=pickle.load(modelFile)
            modelsCount[realNumberOfStates(model)]+=1
    genomeAndRealNrOfStates[fastaName]=modelsCount

if __name__=="__main__":
    cnc.checkExistenceOfPlotOutDir()
    with open("GC/dumpFile.store","rb") as fileGC:
        genomeToGC=pickle.load(fileGC)
    cnc.forEachModelGroup(pickRealNumberOfStates)
    print(genomeAndRealNrOfStates)
    plotDataGC=[]
    plotData=[]
    plotLabels=[]
    for genomeName in genomeToGC:
        plotDataGC.append(genomeToGC[genomeName])
        plotData.append(genomeAndRealNrOfStates[genomeName])
        plotLabels.append(genomeName)
    plotData=np.array(plotData)

    for i in range(1,4):
        plt.scatter(plotDataGC, plotData[:,i])
        plt.grid(True)

        for j in range(len(plotLabels)):
            an=plt.annotate(plotLabels[j], [plotDataGC[j], plotData[j,i]])
            an.set_fontsize(4)
            an.set_rotation(45)
        plt.ylabel("Number of models with {} real regions.".format(i))
        plt.xlabel("GC")
        plt.savefig("Wykresy/realRegions{}.png".format(i), dpi=500)
        plt.show()

    
