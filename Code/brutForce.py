import time
import numpy as np
import pomegranate as pom
import pickle, datetime
import multiprocessing
import os, sys, queue
import readSequenceFunctions as rsf
from baseHMM import endDistr, randomNonStopDistr

def BFS(matrix, start, end):
    q=queue.Queue()

    for i in range(len(start)):
        if start[i]==1:
            q.put(i)

    seen=set()
    while not q.empty():
        s=q.get()
        for i in range(len(matrix[s,:])):
            if matrix[s,i]==1 and i not in seen:
                if end[i]==1:
                    return True
                seen.add(i)
                q.put(i)

    return False

def constructTopologyMatrixes(n,a,s,e,arg, checkIntegrity=True):
    matrix=np.zeros(n*n)
    mask=np.array(a//(np.full((n*n),2).cumprod()//2)%2,dtype=bool)
    matrix[mask]=1

    start=np.zeros(n)
    mask=np.array(s//(np.full((n),2).cumprod()//2)%2,dtype=bool)
    start[mask]=1

    end=np.zeros(n)
    mask=np.array(e//(np.full((n),2).cumprod()//2)%2,dtype=bool)
    end[mask]=1

    matrix=matrix.reshape((n,n))

    if (checkIntegrity) and (not BFS(matrix, start, end)):
        if arg.verbose:
            print("Model without path do end:",str(n),str(a),str(s),str(e),file=sys.stderr)
            print("Model ignored.")
        return None

    matrix=np.hstack((end.reshape((-1,1)), matrix))
    matrix=np.vstack((np.zeros((matrix.shape[1])), matrix))
    return(matrix,start)


def createTopolgyOfBFModel(n,a,s,e, arg):
    """
    Function  which create model base on given topology.
    Numbers which describes matrixes, inform us that in given place is an egde
    if and only if this number in binary format has one.
    Arguments:
        n - number of states
        a - description of transition matrix
        s - description of start states
        e - description of end states
    Return:
        model - HMM model with given states and random discrete distribution in states
    """
    top=constructTopologyMatrixes(n,a,s,e,arg,checkIntegrity=True)
    if top is None:
        return None
    matrix,start=top

    distrList=[endDistr(rsf.usedCoding.numberOfSymbols, rsf.usedCoding)]
    distrList=distrList+[randomNonStopDistr(rsf.usedCoding.numberOfSymbols) for i in range(n)]

    end=np.zeros(n+1)
    end[0]=1
    start=np.hstack((np.array([0]),start))

    try:
        model=pom.hmm.HiddenMarkovModel.from_matrix(matrix,distrList,start,end)
    except UnboundLocalError:
        if arg.verbose:
            print("Error when creating HMM model:",str(n), str(a), str(s), str(e), file=sys.stderr)
            print("Model ignored.",file=sys.stderr)
        return None;
    return model;

def createBFModel(n,a,s,e,trainList, arg):
    verbose=arg.train_verbose
    savePrefix=arg.out_dir
    saveSufiks=arg.sufiks
    if os.path.isfile(os.path.join(savePrefix,"m-n"+str(n)+"-a"+str(a)+"-s"+str(s)+"-e"+str(e)+saveSufiks)):
        return;
    if verbose:
        print("Start of creating:\n n: "+str(n)+" a: "+str(a)+" s: "+str(s)+ " e: "+str(e))
    model=createTopolgyOfBFModel(n,a,s,e, arg)
    if model is None:
        return;
    imp=model.fit(trainList, stop_threshold=arg.stop_threshold,
                  verbose=verbose, transition_pseudocount=arg.transition_pseudocount, return_history=True)
    if np.isnan(imp[1].total_improvement[0]):
        if arg.verbose:
            print("NaN model:",str(n), str(a), str(s), str(e), file=sys.stderr)
            print("Model ignored.",file=sys.stderr)
        return
    score=model.summarize(trainList)
    with open(os.path.join(savePrefix,"m-n"+str(n)+"-a"+str(a)+"-s"+str(s)+"-e"+str(e)+saveSufiks),"wb") as file:
        pickle.dump(model,file)
    with open(os.path.join(savePrefix,"m-n"+str(n)+"-a"+str(a)+"-s"+str(s)+"-e"+str(e)+saveSufiks+".result"),"wb") as file:
        pickle.dump(score,file)

def wrapperOfCreateBFModel(q:multiprocessing.Queue, arg, trainList):
#    plik=open(os.path.join(arg.out_dir,str(datetime.datetime.now())+".debug"),"w")
    while not q.empty():
        try:
            n,a,s,e=q.get(True, 5)
            createBFModel(n,a,s,e,trainList, arg)
        except multiprocessing.queues.Empty:
            if arg.verbose:
                print("Queue empty.")
            break

def brutForce(stateNumber, trainList, arg):
    """
    Take state number, list with encoded sequences and structure with
    program arguments and check all models for given state number.
    """
    if arg.verbose:
        print("Start brute force.")
    n=stateNumber
    inputData=multiprocessing.Queue()
    if arg.verbose:
        print("Queue created.")
    for a in range(1,2**(n*n)):
        for s in range(1,2**n):
            for e in range(1,2**n):
                inputData.put((n,a,s,e))
    if arg.verbose:
        print("Inserted all possible topogies into queue.")
        print("Queue length:", inputData.qsize())

    process=[]
    for i in range(arg.threads):
        process.append(multiprocessing.Process(target=wrapperOfCreateBFModel, args=(inputData,arg, trainList)))
        process[-1].start()
    if arg.verbose:
        print("All proces started.")
    for p in process:
        p.join()

    if arg.verbose:
        print("All process ended.")
    inputData.close()
    inputData.join_thread()
    if arg.verbose:
        print("Queue closed.")


def readResultsOfBF(n,arg):
    models=[]
    for a in range(1,2**(n*n)):
        for s in range(1,2**n):
            for e in range(1,2**n):
                try:
                    modelName="m-n"+str(n)+"-a"+str(a)+"-s"+str(s)+"-e"+str(e)
                    with open(os.path.join(arg.in_dir,modelName+arg.sufiks),"rb") as file:
                        score=pickle.load(file)
                    bestScore=score
                    bestModel=(a,s,e)
                    models.append((bestScore,modelName,bestModel))
                except FileNotFoundError:
                    if arg.verbose:
                        print("FileNotFoundError. Model: ", (a,s,e),"ignored.")
    models.sort()
    if arg.number_of_best>len(models):
        print("Warning: Number of best models to print decreased to total number of models.")
        arg.number_of_best=len(models)
    for i in range(1,arg.number_of_best+1):
        print(models[-i][0], models[-i][1])
    return

def BIC(parameters, dataSize, logML):
    return parameters*np.log(dataSize)-2*logML

def analizeResultsWithBIC(n, dataSize, arg):
    #one degree because internal state cannot have stop codon
    #second because it is determined by rest of symbols
    degreesOfFreedomInState=rsf.usedCoding.numberOfSymbols-2
    models=[]
    if arg.verbose:
        print("State numbers:",n, file=sys.stderr)
        print("Degrees of freedom in one internal state:",degreesOfFreedomInState, file=sys.stderr)
        print("Data size:",dataSize, "ln:",np.log(dataSize), file=sys.stderr)
    for a in range(1,2**(n*n)):
        for s in range(1,2**n):
            for e in range(1,2**n):
                try:
                    modelName="m-n"+str(n)+"-a"+str(a)+"-s"+str(s)+"-e"+str(e)
                    with open(os.path.join(arg.in_dir,modelName+arg.sufiks+".result"),"rb") as file:
                        score=pickle.load(file)
                    with open(os.path.join(arg.in_dir,modelName+arg.sufiks),"rb") as file:
                        modelPickled=pickle.load(file)
                    parameters=0
                    matrix=np.matrix(np.matrix(modelPickled.dense_transition_matrix(), dtype=bool), dtype=int)
                    vector=np.sum(matrix,axis=1)
                    vector-1
                    #we add +1 to sum of vector, because number of out edges from end is 0, so after
                    #above substraction we have -1 degree of freedom of out edges from end
                    parameters+=np.sum(vector)+1
                    parameters+=n*degreesOfFreedomInState
                    score=BIC(parameters,dataSize,score)
                    model=(a,s,e)
                    models.append((score,modelName,model))
                except FileNotFoundError:
                    if arg.verbose:
                        print("FileNotFoundError. Model: ", (a,s,e),"ignored.", file=sys.stderr)
    models.sort()
    if arg.number_of_best>len(models):
        print("Warning: Number of best models to print decreased to total number of models.")
        arg.number_of_best=len(models)
    for i in range(0,arg.number_of_best):
        print(models[i][0], models[i][1])
    return



def analizeManyBIC(data,dataSize, arg):
    #one degree because internal state cannot have stop codon
    #second because it is determined by rest of symbols
    degreesOfFreedomInState=rsf.usedCoding.numberOfSymbols-2
    models=[]
    if arg.verbose:
        print("Degrees of freedom in one internal state:",degreesOfFreedomInState, file=sys.stderr)
        print("Data size:",dataSize, "ln:",np.log(dataSize), file=sys.stderr)

    for file in os.listdir(arg.in_dir):
        if os.path.splitext(file)[-1]!=arg.sufiks:
            continue
        modelName=file
        with open(os.path.join(arg.in_dir,modelName),"rb") as file:
            modelPickled=pickle.load(file)
        score=modelPickled.summarize(data)
        parameters=0
        matrix=np.matrix(np.matrix(modelPickled.dense_transition_matrix(), dtype=bool), dtype=int)
        vector=np.sum(matrix,axis=1)
        vector-1
        #we add +1 to sum of vector, because number of out edges from end is 0, so after
        #above substraction we have -1 degree of freedom of out edges from end
        parameters+=np.sum(vector)+1
        parameters+=(modelPickled.state_count()-3)*degreesOfFreedomInState
        score=BIC(parameters,dataSize,score)
        models.append((score,modelName))
    models.sort()
    if arg.number_of_best>len(models):
        print("Warning: Number of best models to print decreased to total number of models.")
        arg.number_of_best=len(models)
    for i in range(0,arg.number_of_best):
        print(models[i][0], models[i][1])
    return

