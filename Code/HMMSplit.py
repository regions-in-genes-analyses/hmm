import numpy as np
import pomegranate as pom
import baseHMM
import datetime, argparse, pickle, os, multiprocessing, sys, pprint
import readSequenceFunctions as rsf
import sequenceProcesing as sp
import brutForce as bF
from parseInput import *

class DeflautValues():
    mutationMatrix=np.array([[0.75,0.0625,0.0625,0.125],
                             [0.0625,0.75,0.125,0.0625],
                             [0.0625,0.125,0.75,0.0625],
                             [0.125,0.0625,0.0625,0.75]])
def prepareOutDir(arg):
    arg.out_dir=os.path.abspath(arg.out_dir+"/")
    if os.path.isdir(arg.out_dir):
        return
    if os.path.exists(arg.out_dir):
        raise FileExistsError
    os.makedirs(arg.out_dir)

def createModel(arg, seqListT, generator=None):
    start=np.loadtxt(arg.start)
    end=np.loadtxt(arg.end)
    transMatrix=np.loadtxt(arg.transition_matrix)
    for copNum in range(arg.copy_number):
        if arg.name is not None:
            if arg.copy_number!=1:
               name=arg.name+"-modelNumber"+str(copNum)
            else:
               name=arg.name
        else:
            name=str(datetime.datetime.now())
        outFile=open(os.path.join(arg.out_dir,name+arg.sufiks),"wb")
        if arg.distributions is None:
            distr=baseHMM.createRandomDistrList(transMatrix.shape[0], rsf.usedCoding, generator)
        else:
            distr=readDistributions(arg.distributions)
        model=pom.HiddenMarkovModel.from_matrix(transMatrix,distr,start,end)
        val=model.fit(seqListT, stop_threshold=arg.stop_threshold, verbose=arg.train_verbose)
        pickle.dump(model, outFile)
        outFile.close()

def analizeSequences(arg, seqDict, seqDictT, mutationMatrix):
    with open(arg.model,"rb") as modelFile:
        model=pickle.load(modelFile)
    weights=rsf.createWeightsDict(mutationMatrix)
    if arg.name is not None:
        name=arg.name
    else:
        name=str(datetime.datetime.now())
    outFile=open(os.path.join(arg.out_dir,name+arg.sufiks),"w")
    for seqName in seqDict:
        sequenceScore=model.log_probability(seqDictT[seqName])

        print(seqName,sequenceScore,sep="\t",end="\t",file=outFile)

        result,length,symbols=sp.calculateF(model,seqDict[seqName],seqDictT[seqName],
                                            weights, path_alg=arg.path_alg, verbose=arg.verbose)
        for F,l,s in zip(result,length,symbols):
            print(F,l,s,sep="\t",end="\t",file=outFile)
        print(file=outFile)
    outFile.close()

def retrainModels(q,trainList,arg):
    while not q.empty():
        try:
            path=q.get(True,5)
            with open(os.path.join(arg.in_dir, path),"rb") as file:
                try:
                    model=pickle.load(file)
                except UnboundLocalError:
                    print("Error while loading model: ", path, " Model omitted.",file=sys.stderr)
                    continue
            val=model.fit(trainList, stop_threshold=arg.stop_threshold, verbose=arg.train_verbose, return_history=True)
            if np.isnan(val[1].total_improvement[0]) and arg.verbose:
                print("NaN improvment:",path," Model omitted.",file=sys.stderr)
                continue
            with open(os.path.join(arg.out_dir,path),"wb") as file:
                pickle.dump(model, file)
            with open(os.path.join(arg.out_dir,path+".result"),"wb") as file:
                pickle.dump(val[1].log_probabilities[-1], file)
        except multiprocessing.queues.Empty:
            return

def retrain(trainList, arg):
    if arg.verbose:
        print("Started retrain.")
    q=multiprocessing.Queue()
    sufLength=len(arg.sufiks)
    for path in os.listdir(arg.in_dir):
        if path[-sufLength:]==arg.sufiks:
            q.put(path)
    if arg.verbose:
        print("Inserted paths to queue.")

    process=[]
    for i in range(arg.threads):
        process.append(multiprocessing.Process(target=retrainModels, args=(q,trainList,arg)))
        process[-1].start()
    if arg.verbose:
        print("Started processes.")
    for proc in process:
        proc.join()
    if arg.verbose:
        print("Ended processes.")

    q.close()
    q.join_thread()
    if arg.verbose:
        print("Closed queue")

def printModels(arg):
    for path in arg.files:
        with open(path, "rb") as file:
            model=pickle.load(file)
        if arg.term_out:
            print(model.dense_transition_matrix())
            print(model.states)
        if arg.dot:
            pp=pprint.PrettyPrinter(width=17)
            dotfile=open(os.path.join(arg.out_dir, os.path.splitext(os.path.basename(path))[0]+".dot"),"w")
            print("strict digraph {", file=dotfile)
            for state in model.states:
                if state.name=="None-start":
                    state.name="START"
                    print("START", file=dotfile)
                elif state.name=="None-end":
                    state.name="END"
                    print("END", file=dotfile)
                elif state.distribution is not None:
                    print(state.name, "[label=\"",end=" ", file=dotfile)
                    if state.distribution.name=="DiscreteDistribution":
                        for sym in state.distribution.parameters[0]:
                            print(sym,":","{:5f}".format(state.distribution.parameters[0][sym]), end="\\n", file=dotfile)
                    print("\"]", file=dotfile)
            matrix=model.dense_transition_matrix()
            for i in range(matrix.shape[0]):
                for j in range(matrix.shape[1]):
                    if matrix[i,j]!=0:
                        print(model.states[i].name,"->",model.states[j].name,"[label=\"",
                              "{:.5f}".format(matrix[i,j]),"\"]",file=dotfile)
            print("}",file=dotfile)



def prepareGenome(arg, seqDict, seqDictT):
    outFile=open(os.path.join(arg.out_dir,str(datetime.datetime.now())+arg.sufiks),"wb")
    pickle.dump((seqDict,seqDictT),outFile)
    outFile.close()

def printGenomeInMachineFormat(arg, seqDictT):
    """
    Print seqDictT to file with one sequence in one line and one space between symbols
    to create file which is easy to parse by other programs.
    """
    if arg.name is None:
        outFile=open(os.path.join(arg.out_dir,str(datetime.datetime.now())+arg.sufiks),"w")
    else:
        outFile=open(os.path.join(arg.out_dir,arg.name+arg.sufiks),"w")
    for seqName in seqDictT:
        for symbol in seqDictT[seqName][:-1]:
            print(symbol, end=' ', file=outFile)
        print(-1,file=outFile)
    outFile.close()

def main():
    arg,parser=parseArguments()
    prepareOutDir(arg)

    ################
    #Mutation matrix
    ###############
    if arg.mutation_matrix is None:
        mutationMatrix=DeflautValues.mutationMatrix
    else:
        mutationMatrixFile=open(arg.mutation_matrix, "r")
        mutationMatrix=np.loadtxt(mutationMatrixFile)
        mutationMatrixFile.close()

    ############
    #Coding
    ############
    rsf.usedCoding=rsf.Coding(arg.coding, mutationMatrix=mutationMatrix, maxNumberOfGroups=arg.group_number)
    if arg.verbose:
        print("Coding type:",rsf.usedCoding.type)
        print("Stop codons symbol:",rsf.usedCoding.stopCodonsSymbol)
        print("Number of symbols:",rsf.usedCoding.numberOfSymbols)
        print("Threshold:",rsf.usedCoding.threshold)
        print(rsf.usedCoding.coding)


    ############
    #Random number generator
    ############
    if arg.seed is not None:
        generator=np.random.default_rng(arg.seed)
    else:
        generator=None

    ############
    #Input genome files
    ############
    if arg.preprocesed_genomes:
        if len(arg.files)!=1:
            raise RuntimeError("When using preprocesed genome only one file should be passed on program.")
        preprocesedGenomesFile=open(arg.files[0],"rb")
        seqDict,seqDictT=pickle.load(preprocesedGenomesFile)
        preprocesedGenomesFile.close()
    elif not arg.brut_force_analize and not arg.print:
        if arg.files is None or len(arg.files)==0:
            raise RuntimeError("There is no file with sequences.")
        seqDict,seqDictT=rsf.prepareDictionary(arg.files)

    ############
    #Auto stop threshold
    ############
    if arg.auto_threshold:
        arg.stop_threshold=arg.stop_threshold_per_seq*len(seqDictT)

    ###########
    #Mode
    ##########
    if arg.create:
        if arg.start is None or arg.end is None or arg.transition_matrix is None:
            parser.error("Not enough information to create HMM model.")
        createModel(arg, list(seqDictT.values()), generator=generator)
    elif arg.analize:
        if arg.model is None:
            parser.error("Not enough information to analize genomes.")
        analizeSequences(arg, seqDict, seqDictT, mutationMatrix)
    elif arg.brut_force:
        if arg.state_number is None:
            parser.error("Not enough information to check all topologies.")
        bF.brutForce(arg.state_number, list(seqDictT.values()), arg)
    elif arg.retrain:
        if arg.in_dir is None:
            parser.error("Not enough information to retrain models.")
        retrain(list(seqDictT.values()),arg)
    elif arg.prepare_genome:
        prepareGenome(arg, seqDict, seqDictT)
    elif arg.brut_force_analize:
        if arg.state_number is None:
            parser.error("Not enough information to check all topologies.")
        bF.readResultsOfBF(arg.state_number,arg)
    elif arg.bfatoBIC:
        if arg.state_number is None:
            parser.error("Not enough information to check all topologies.")
        bF.analizeResultsWithBIC(arg.state_number,len(seqDict),arg)
    elif arg.analizeManyBIC:
        bF.analizeManyBIC(list(seqDictT.values()),len(seqDict),arg)
    elif arg.print:
        printModels(arg)
    elif arg.print_genome_machine:
        printGenomeInMachineFormat(arg, seqDictT)

def parseArguments():
    parser=argparse.ArgumentParser(description="Program to create HMM models and spliting with them sequence into regions.")


    funArg=parser.add_argument_group("functional arguments")
    funArgGrp=funArg.add_mutually_exclusive_group(required=True)
    funArgGrp.add_argument("--create", action="store_true",
                           help="Create new HMM model")
    funArgGrp.add_argument("--analize", action="store_true",
                           help="Analize sequences with help of existing HMM model")
    funArgGrp.add_argument("--retrain", action="store_true",
                           help="Retrain models with modified genome files.")
    funArgGrp.add_argument("--brut-force", action="store_true",
                           help="Find best topology checking all possible.")
    funArgGrp.add_argument("--brut-force-analize", action="store_true",
                           help="Analize results of trained brut force models.")
    funArgGrp.add_argument("--bfatoBIC", action="store_true",
                           help="Analize trained brut force models base on BIC.")
    funArgGrp.add_argument("--prepare-genome", action="store_true",
                           help="Create dataset from genome files")
    funArgGrp.add_argument("--print", action="store_true",
                           help="Print representation of given model.")
    funArgGrp.add_argument("--print-genome-machine", action="store_true",
                           help="Print encoded genome in machine format without stop symbol at end.")
    funArgGrp.add_argument("--analizeManyBIC", action="store_true",
                           help="Analize trained models stored in one dictionary (similar to --bfatoBIC, but for any file name and there is no need for having .results files).")

    createArg=parser.add_argument_group("creating model options")
    createArg.add_argument("-tm","--transition-matrix",
                           help="Path to file describing transition matrix")
    createArg.add_argument("-s","--start",
                           help="Path to file which describe states from which HMM could start.")
    createArg.add_argument("-e","--end",
                           help="Path to file which describe states from which HMM could end.")
    createArg.add_argument("--distributions", default=None,
                           help="Path to file which describe start distributions, if not set distribution will be random")
    createArg.add_argument("-cn", "--copy-number", type=int, default=1,
                            help="Number of models which should be fitted to data.")

    tresArgGrp=createArg.add_mutually_exclusive_group()
    tresArgGrp.add_argument("-st", "--stop-threshold", type=int, default=10,
                           help="Treshold by which HMM fitting should be stopped.")
    tresArgGrp.add_argument("-at", "--auto-threshold", action="store_true",
                            help="Set stop threshold based on number of sequences.")
    createArg.add_argument("-tps", "--stop-threshold-per-seq", type=float, default=0.001,
                            help="Avarange change on one sequence by with fitting HMM should stop.")

    analizeArg=parser.add_argument_group("analize options")
    analizeArg.add_argument("-m","--model",
                            help="Path to HMM model which should be use to analize genome.")
    analizeArg.add_argument("--path-alg", choices=["viterbi", "map"], default="viterbi",
                            help="Algorithm which should be used to determine regions")

    brutArg=parser.add_argument_group("brut force options")
    brutArg.add_argument("-sn","--state-number", type=int, default=3,
                         help="Number of non-stop states which should be considered.")
    brutArg.add_argument("-t","--threads", type=int, default=1,
                         help="Number of threads which should be used when checking all models.")
    brutArg.add_argument("-tp","--transition-pseudocount", type=int, default=0,
                         help="Number which should be added to usage of each edge in HMM.")
    brutArg.add_argument("-nb","--number-of-best", type=int, default=1,
                         help="How many best models should be returned.")


    printArg=parser.add_argument_group("print options")
    printArg.add_argument("--term-out", default=False, action="store_true",
                          help="Print model into terminal.")
    printArg.add_argument("--dot", default=False, action="store_true",
                          help="Create dot file with printed model.")

    parser.add_argument("--files", nargs="+",
                        help="Paths to fasta input files.")
    parser.add_argument("--coding", choices=[1,2,3,4,5], type=int, default=1,
                           help="Coding which should be used")
    parser.add_argument("-mm","--mutation-matrix", default=None,
                        help="Path to file describing mutation matrix")
    parser.add_argument("--seed", type=int, default=None,
                        help="Seed for random number generator")
    parser.add_argument("-od","--out-dir",default=".",
                        help="Path do directory in which should be output saved")
    parser.add_argument("-id","--in-dir", default=None,
                        help="Directory with files for retrain, brut force analize or bfatoBIC.")
    parser.add_argument("-tv","--train-verbose", default=False, action="store_true",
                        help="Use verbose mode when fitting HMM")
    parser.add_argument("-pd","--preprocesed-genomes", action="store_true",
                        help="File contain preprocesed genome returned by --prepare-genome")
    parser.add_argument("--sufiks", type=str, default="",
                        help="Sufiks which whould be used with output files.")
    parser.add_argument("--verbose", default=False, action="store_true",
                        help="Print additional message.")
    parser.add_argument("--name", default=None,
                        help="Name of output files.")
    parser.add_argument("-gn","--group-number", type=int, default=1,
                        help="Number of non stop goups in coding 5.")


    arg=parser.parse_args()
    return arg,parser


if __name__=="__main__":
    main()

