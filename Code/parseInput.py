import numpy as np
import pomegranate as pom
def readDistributions(path):
    """
    Take path to file which describe distributions and return list of dictionaries.

    File should have the same number of rows as number of states,
    and there should be so many columns, as symbols in coding.
    Number on row "i" and column "j" is probability of generating
    j-th symbol in i-th state.
    """

    file=open(path,"r")
    numbers=np.loadtxt(path)
    distr=[]
    for w in range(numbers.shape[0]):
        D={}
        for k in range(numbers.shape[1]):
            D[k]=numbers[w,k]
        distr.append(pom.DiscreteDistribution(D))
    return distr
