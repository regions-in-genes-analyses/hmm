import numpy as np
import pomegranate as pom
import pickle
import readSequenceFunctions as rsf

def testuj(model1, model2, testList):
    """Take two models and test list and evaluate probabilieties on each sequence from test list,
    calculate diffrence, and on the end the mean of these differences. Values greater than zero
    mean that first model is better."""
    probabilities=[]
    for seq in testList:
        prob1=model1.log_probability(seq)
        prob2=model2.log_probability(seq)
        probabilities.append([prob1,prob2])

    P=np.array(probabilities)
    Prem=P[:,1]-P[:,0]
    return Prem.mean()

def endDistr(symbolsCount, usedCoding):
    """Crete DiscreteDistribution which have possible generated values from 0 to symbolsCount-1 (itegers),
    and generate only stop codons."""
    d={}
    for i in range(symbolsCount):
        d[i]=0
    d[usedCoding.stopCodonsSymbol]=1
    return pom.DiscreteDistribution(d)


def randomNonStopDistr(symbolsCount, generator=None):
    """Create random DiscreteDistribution, which create integers from 0 to symbolsCount-1,
    without integer denoting stop codons. Optionaly take random nuber generator from numpy."""
    d={}
    if generator is not None:
        generatedList=generator.uniform(size=symbolsCount)
    else:
        generatedList=np.random.rand(symbolsCount)
    generatedList[rsf.usedCoding.stopCodonsSymbol]=0
    sumOfList=np.sum(generatedList)
    generatedList/=sumOfList
    for i in range(symbolsCount):
        d[i]=generatedList[i]
    return pom.DiscreteDistribution(d)


def createRandomDistrList(statesCount, usedCoding:rsf.Coding, generator=None):
    """Create random DiscreteDistribution list with end state on the begining.
    Arguments:
        statesCount - count of DiscreteDistribution in list
        usedCoding - object which describe used coding (constructed by readSequenceFunctions module)
        generator=None - numpy random number generator
    Return:
        list of DiscreteDistribution
    """
    symbolsCount=usedCoding.numberOfSymbols
    distributionList=[endDistr(symbolsCount, usedCoding)]
    distributionList+=[randomNonStopDistr(symbolsCount, generator=generator) for i in range(statesCount-1)]
    return distributionList
